package cs.mad.flashcards.entities

data class FlashcardSet(var title: String) {
        fun Title():List<FlashcardSet> {
            val card1 = FlashcardSet("Color_1")
            val card2 = FlashcardSet("Color_2")
            val card3 = FlashcardSet("Color_3")
            val card4 = FlashcardSet("Color_4")
            val card5 = FlashcardSet("Color_5")
            val card6 = FlashcardSet("Color_6")
            val card7 = FlashcardSet("Color_7")
            val card8 = FlashcardSet("Color_8")
            val card9 = FlashcardSet("Color_9")
            val card10 = FlashcardSet("Color_10")
            return listOf(card1, card2, card3, card4, card5, card6, card7, card8, card9, card10)

        }
    }
