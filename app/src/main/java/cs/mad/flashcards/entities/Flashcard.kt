package cs.mad.flashcards.entities

data class Flashcard(var term:String, var def:String) {
    fun cards(): List<Flashcard> {
        val card1 = Flashcard("What is blue", "whale")
        val card2 = Flashcard("What is red", "ball")
        val card3 = Flashcard("What is green", "grass")
        val card4 = Flashcard("What is yellow", "sun flower")
        val card5 = Flashcard("What is orange", "orange")
        val card6 = Flashcard("What is black", "cat")
        val card7 = Flashcard("What is white", "cloud")
        val card8 = Flashcard("What is pink", "flamingo")
        val card9 = Flashcard("What is purple", "grape")
        val card10 = Flashcard("What is red", "ant")
        return listOf(card1, card2, card3, card4, card5, card6, card7, card8, card9, card10)

    }
}


